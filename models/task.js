const mongoose = require('mongoose')

const taskSchema = mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})


module.exports = mongoose.model("Tasks", taskSchema)