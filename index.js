const express = require('express')
const mongoose = require('mongoose')
const taskRoute = require('./routes/taskRoute')

const app = express()
const port = 4000

app.use(express.json())
app.use(express.urlencoded({extended : true}))


mongoose.connect('mongodb+srv://rjnebrija:superman123@cluster0.flsml.mongodb.net/B157_to-do?retryWrites=true&w=majority', {
	useNewUrlParser : true,
	useUnifiedTopology : true
})

const db = mongoose.connection

db.on('error' , console.error.bind(console, "Connection Error"))
db.once('open', ()=> console.log("We're connected to the database"))

app.use('/tasks', taskRoute)

app.listen(port, ()=> console.log(`Server now listening at port ${port}`))